<?php

namespace TorneLIB\Errors;

use Throwable;

class ExceptionHandler extends \Exception
{
    private $MESSAGE;

    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
